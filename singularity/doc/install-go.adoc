== Instalación del Compilador GO
// author information and configuration
:author: Miguel Angel Mercado Velasco
:email: skyllerx@gmail.com
:revdate: 2020-04-19
:toc:

// All URL's
:web-go: https://golang.org/
:dl-go: https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
:install-snap: https://snapcraft.io/docs/installing-snapd

// Documentación
{web-go}[GO] es un lenguaje de programación concurrente y compilado inspirado en la sintaxis de *C*, que intenta ser dinámico como *Python* y con el rendimiento de *C* o *C++*. Esté nuevo lenguaje de programación empezó a ser desarrollado en septiembre de 2007 por Robert Griesemer, Rob Pike y Ken Thompson y es lanzado por Google en noviembre de 2009.

{web-go}[GO] nos aporta una sintaxis sencilla, fácil de interpretar con la potencia que nos ofrece un lenguaje fuertemente tipado y compilado, además las goroutines (concurrencia) son canales de comunicación basados en el lenguaje CSP (Comunicación de Procesos Secuenciales) que es mucho más seguro y fácil de usar que los sistemas predominantes basados en bloqueos de pthreads.

El lenguaje de {web-go}[GO] tiene tipos y métodos y permite un estilo de programación orientado a objetos pero no existe una jerarquía de objetos, por lo tanto, no existe la herencia, además el concepto de “interfaz“ es fácil de usar y en muchos sentidos es más general. También admite la tipificación dinámica de datos conocida como *Duck Typing* presente en multitud de lenguajes dinámicos.

En conclución podemos mencionar que el lenguaje de {web-go}[GO] se basa en los tres pilares fundamentales: eficiencia, rapidez de ejecución y facilidad de lenguaje, características que facilitan el uso del lenguaje por los desarrolladores. 



=== Prerequisitos
//tag::prereq[]
Para su instalación Go solo se requiere Linux 2.6.23 o superior con la libreria *glibc* nadamas. La siguiente tabla muestra la instalación de los distintos gestores de paquetes:

.Instalacion de glibc
[options = "header, footer"]
|======
|apt|yum|pacman
|sudo apt-get install glibc-source|sudo yum install glibc-devel|sudo pacman -S glibc
|======
// end::prereq[]

=== Instalación
// tag::install[]
Primero se descarga el paquete de {dl-go}[GO], lo extraemos en un directorio de preferencia, y agregamos al *PATH* el directorio *bin* de GO.

.Instalación de GO por Descarga

[source,bash]
--
$ wget https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
$ sudo tar -C /opt -xzf go1.14.2.linux-amd64.tar.gz
$ rm go1.14.2.linux-amd64.tar.gz
$ echo 'export PATH=$PATH:/opt/go/bin' >> ~/.bashrc
$ . ~/.bashrc
--

NOTE: Otra manera de instalar GO es con el gestor de paquetes {install-snap}[snap]

// end::install[]

=== Verificación
// tag::test[]
Por último verificamos que tengamos instalado *GO*.

.Version de GO

[source,bash]
--
$ go version
--

También se puede probar si GO esta instalado solo creando un simple programa que imprima un *Hola, Mundo* (hello.go) con el siguiente contenido:

[source,bash]
--
package main

import "fmt"

func main() {
	fmt.Printf("Hola, Mundo\n")
}
--

Lo compilamos y ejecutamos

[source,bash]
--
$ go build hello.go
$ ./hello.go
--

// end::test[]
