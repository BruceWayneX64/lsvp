#!/bin/bash

WEB="ftp://ftp.ncbi.nlm.nih.gov/blast/db"
DIR="/home/database"
MCB="16SMicrobial"
SWP="swissprot"

mkdir -p $DIR/{nt,16SMicrobial,swissprot}

is_file_exists(){
    local f="$1"
    [[ -f "$f" ]] && return 0 || return 1
}

function dl_database {
  CF=$1.tar.gz
  echo ==================================================================================================
  if ( is_file_exists "$CF" )
  then  
    echo El archivo $1 ya esta descargado
  else
    echo Iniciando descarga de archivo $1
    wget $WEB/$CF 
    echo Finalizó descarga con exito
  fi
  echo Descomprimiendo...
  tar -xf $CF -C $DIR/$2
  echo Finalizó, El Archivo $1 ya esta descomprimido en el directorio $DIR/$2/ 
  echo ==================================================================================================
}

dl_database $MCB $MCB
dl_database $SWP $SWP

for i in {0..77}
do
  if [ $i -le 9 ] 
  then
    dl_database nt.0$i nt
  else
    dl_database nt.$i n nt
  fi
done
